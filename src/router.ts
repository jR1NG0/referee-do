import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/proxFechas',
      name: 'proxFechas',
      component: () => import('./views/ProxFechas.vue'),
    },
    {
      path: '/jugadores',
      name: 'jugadores',
      component: () => import('./views/Jugadores.vue'),
    },
    {
      path: '/agregarJugador',
      name: 'agregarJugador',
      component: () => import('./views/AgregarJugador.vue'),
    },
    {
      path: '/agregarFecha',
      name: 'agregarFecha',
      component: () => import('./views/AgregarFecha.vue'),
    },
    {
      path: '/estadisticas',
      name: 'estadisticas',
      component: () => import('./views/Estadisticas.vue'),
    }
  ],
});
